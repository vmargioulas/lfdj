(defproject lfdj "1.0.0-SNAPSHOT"
  :description "Learning from data algorithms using clojure"
  :dependencies [[org.clojure/clojure "1.3.0"]
                 [incanter "1.3.0"]])