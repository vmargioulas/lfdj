(ns lfdj.core
  (:use [incanter.core :only [matrix plus mult trans mmult view dataset solve]]
        [incanter.stats :only [sample-uniform mean]]))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Algorithms: PLA, pocket, linear regression

(defn zero-w [training-set]
  (repeat (inc (count (:x (first training-set)))) 0.0))

(defn classify [w x]
  (Math/signum (mmult (trans w) (cons 1 x))))

(defn misclassified? [w {:keys [x y] :as training-sample}]
  (when (not (== (classify w x) y))
    training-sample))

(defn pla [training-set w]
  (when-let [{:keys [x y]} (some (partial misclassified? w)
                                 (shuffle training-set))]
    (plus w (mult y (cons 1 x)))))

(defn pla-seq [training-set w]
  (take-while (comp not nil?) (iterate (partial pla training-set) w)))

(defn hypothesis-h [w]
  (partial classify w))

(defn hypothesis-h-error [h test-samples]
  (let [misclassified-fn #(not (== (:y %) (h (:x %))))
        samples-misclassified (filter misclassified-fn test-samples)]
    (/ (count samples-misclassified) (count test-samples))))

(defn pocket [training-set max-iterations pseq]
  (apply min-key #(hypothesis-h-error (hypothesis-h %) training-set)
         (take max-iterations pseq)))

(defn lreg [training-set]
  (let [X (matrix (map #(cons 1 (:x %)) training-set))
        pseudo-inverse (mmult (solve (mmult (trans X) X)) (trans X))]
    (mmult pseudo-inverse (map :y training-set))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Experiment

(defn experiment [learn-fn {:keys [train-data test-data]}]
  {:train-data train-data
   :test-data test-data
   :w (learn-fn train-data)})

(defn Ein [{:keys [train-data w]}]
  (hypothesis-h-error (hypothesis-h w) train-data))

(defn Eout [{:keys [test-data w]}]
  (hypothesis-h-error (hypothesis-h w) test-data))

(defn mean-value [fn times]
  (mean (repeatedly times fn)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Sample Data

(defn sample-point []
  (apply vector (sample-uniform 2 :min -1 :max 1)))

(defn sample-points [n]
  (repeatedly n sample-point))

(defn target-sample [target-fn sample]
  {:x sample, :y (target-fn sample)})

(defn target-set [target-fn samples]
  (map (partial target-sample target-fn) samples))

(defn add-noise [data]
  (map #(assoc % :y (* -1 (:y %))) data))

(defn percent-split [persent sq]
  (split-at (Math/round (* (count sq) (float (/ persent 100))))
            (shuffle sq)))

(defn add-percent-noise [persent train-data]
  (let [[to-add-noise-data rest-data] (percent-split persent train-data)]
    (shuffle (concat (add-noise to-add-noise-data) rest-data))))

(defn transform-set [transform-fn s]
  (map #(assoc % :x (transform-fn (:x %)))
       s))

(defn sample-data [in-sample-N out-sample-N create-data-fn]
  {:train-data (create-data-fn in-sample-N)
   :test-data (create-data-fn out-sample-N)})


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Target fns

(defn line-fn [[x1 y1] [x2 y2]]
  (let [slope (/ (- y2 y1) (- x2 x1))]
    #(+ (* slope (- % x1)) y1)))

(defn line-target-fn [p1 p2]
  (let [fy (line-fn p1 p2)]
    (fn [[px py]]
      (Math/signum (- py (fy px))))))

(defn nonlinear-target [[x1 x2]]
  (Math/signum (+ (* x1 x1) (* x2 x2) -0.6)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Target Data

(defn linear-set-fn [p1 p2]
  (fn [N]
   (->> (sample-points N)
        (target-set (line-target-fn p1 p2)))))

(defn nonlinear-noisy-set [N]
  (->> (sample-points N)
       (target-set nonlinear-target)
       (add-percent-noise 10)))

(defn point-transform [[x1 x2]]
  [x1 x2 (* x1 x2) (* x1 x1) (* x2 x2)])

(defn nonlinear-transformed-noisy-set [N]
  (->> (nonlinear-noisy-set N)
       (transform-set point-transform)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Homework 2 Experiments

(defn linear-experiment [inN outN]
  (->> (linear-set-fn (sample-point) (sample-point))
       (sample-data inN outN)
       (experiment lreg)))

(defn pla-experiment [{:keys [train-data w] :as experiment}]
  (let [pseq (pla-seq train-data w)]
    (assoc experiment :w (last pseq) :iterations (count pseq))))


(defn nonlinear-experiment [inN outN]
  (->> nonlinear-noisy-set
       (sample-data inN outN)
       (experiment lreg)))

(defn nonlinear-transform-experiment [inN outN]
  (->> nonlinear-transformed-noisy-set
       (sample-data inN outN)
       (experiment lreg)))

(defn q5-answer []
  (mean-value #(Ein (linear-experiment 100 1000)) 1000))

(defn q6-answer []
  (mean-value #(Eout (linear-experiment 100 1000)) 1000))

(defn q7-answer []
  (mean-value #(:iterations (pla-experiment (linear-experiment 10 1000)))
              1000))
(defn q8-answer []
  (mean-value #(Ein (nonlinear-experiment 1000 1000)) 1000))

(defn q9-answer []
  (:w (nonlinear-transform-experiment 1000 1000)))

(defn q10-answer []
  (mean-value #(Eout (nonlinear-transform-experiment 1000 1000)) 1000))


